﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System.ComponentModel;
using System.Text;
using System.Security.Cryptography.Xml;
using Microsoft.AspNetCore.Authorization;

namespace primera_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class PersonaController : ControllerBase
    {
       static List<Persona> personas = new List<Persona>
        {
            new () { Nombre = "Daniel", Apellido = "Palencia", Dpi= 19929933,  Sexo = "Masculino", Edad = 18, Profesion = "ingeniero"},
            new () { Nombre = "Manuel", Apellido = "Cafe", Dpi =2344444, Sexo = "Masculino", Edad = 18, Profesion = "ingeniero" },
            new (){ Nombre = "Melany", Apellido = "Paz", Dpi = 435677333, Sexo = "Femenino", Edad = 18, Profesion = "ingeniera" },
            new (){ Nombre = "Daniel", Apellido = "Ibañez", Dpi = 129299453, Sexo = "Masculino", Edad = 18, Profesion = "ingeniero" }
        };

        [HttpGet(Name = "GetPersona")]
        public IEnumerable<Persona> GetPersona()
        {
            return personas;
        }
         
        [HttpPut(Name = "PutPersona")]
        public IEnumerable<Persona> PutPersona([FromBody]Persona persona)
        {

            foreach (var item in personas.Where(x => x.Dpi == persona.Dpi))
            {
                item.Nombre = persona.Nombre;
                item.Apellido = persona.Apellido;
                item.Sexo = persona.Sexo;
                item.Edad = persona.Edad;
                item.Profesion = persona.Profesion;
            }
            return personas;
         }

        [HttpDelete(Name = "DeletePersona")]
        public IEnumerable<Persona> DeletePersona(double dpi)
        {
            personas.RemoveAll(x => x.Dpi.Equals(dpi));
            return personas;
        }

        [HttpPost]
        public IEnumerable<Persona> PostPersona([FromBody]Persona persona)
        {
            Persona nuevaPersona = new Persona
            {

                Nombre = persona.Nombre,
                Apellido = persona.Apellido, 
                Dpi = persona.Dpi, 
                Sexo = persona.Sexo, 
                Edad = persona.Edad,
                Profesion = persona.Profesion
            };
            personas.Add(nuevaPersona);
            return personas;
        }

        [HttpPost("Name")]
        public string PostName(string name)
        {
            string hash = "llave";
            byte[] data = UTF8Encoding.UTF8.GetBytes(name);

            MD5 md5 = MD5.Create();
            TripleDES tripldes =TripleDES.Create();

            tripldes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            tripldes.Mode = CipherMode.ECB;

            ICryptoTransform transform = tripldes.CreateEncryptor();
            byte[] result = transform.TransformFinalBlock(data, 0, data.Length);

            return Convert.ToBase64String(result);
        }

        [HttpPost("Desencrypt")]
        public string Desencrypt(string encriptado)
        {
            string hash = "llave";
            byte[] data = Convert.FromBase64String(encriptado);

            MD5 md5 = MD5.Create();
            TripleDES tripldes =TripleDES.Create();

            tripldes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            tripldes.Mode = CipherMode.ECB;
            ICryptoTransform transform = tripldes.CreateDecryptor();
            byte[] result = transform.TransformFinalBlock(data, 0, data.Length);
            return UTF8Encoding.UTF8.GetString(result);
        }
    }
}


