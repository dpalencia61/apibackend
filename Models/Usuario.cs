﻿namespace primera_api.Models
{
    public class Usuario
    {
        public string id { get; set; }
        public string usuario { get; set; }
        public string passrowd { get; set; }
        public string rol { get; set; }
        public string idUsuario { get; private set; }

        public static List<Usuario> DB()
        {
            var list = new List<Usuario>()
            {
                
                new Usuario
                {
                    idUsuario = "1",
                    usuario = "Mateo",
                    passrowd = "123.",
                    rol = "empleado",

                },

                 new Usuario
                {
                    idUsuario = "2",
                    usuario = "Daniel",
                    passrowd = "123.",
                    rol = "administrador",

                },

                  new Usuario
                {
                    idUsuario = "3",
                    usuario = "Mateo",
                    passrowd = "123.",
                    rol = "assesor",

                },
                   new Usuario
                {
                    idUsuario = "4",
                    usuario = "Jorge",
                    passrowd = "123.",
                    rol = "empleado",

                }

            };

            return list;
       
        }
    

    }
}
